function raiting(className, classFull, classHalf, classEmpty) {
    var countElement = document.getElementsByClassName(className).length;

    for(var i = 0; i < countElement; i++){
        document.getElementsByClassName(className)[i].setAttribute("onclick", "clickStar(this, event, '" + classFull + "', '" + classHalf + "', '" + classEmpty + "')");
    }
}

function clickStar(element, event, classFull, classHalf, classEmpty){
    var inputValue;
    var target = event.target;
    var child = element.firstElementChild.tagName;
    
    classFull = classFull.split(' ');
    classHalf = classHalf.split(' ');
    classEmpty = classEmpty.split(' ');

    if( target.tagName == child ){
        if( target.classList.contains(...classFull) ) {

            //Цикл удаляющий классы 'active' ко всем 'child' до выбранной зведзы
            for( var i = element.children.length - 1; i >= 0; i-- ) {
                
                var childItem = element.children[i];
                
                if( childItem.classList.contains(...classFull) || childItem.classList.contains(classHalf) ){
                    childItem.className = '';
                    childItem.classList.add(...classEmpty);
                }

                if( childItem == target ) {
                    var box = childItem.getBoundingClientRect(),
                        boxLeftHalf = box.x + box.width / 2;

                    if( boxLeftHalf >= event.clientX ) {
                        childItem.classList.remove(...classEmpty);
                        childItem.classList.add(...classHalf);
                        inputValue = i + 0.5;
                    } else {
                        childItem.classList.remove(...classEmpty);
                        childItem.classList.add(...classFull);
                        inputValue = i + 1;
                    }
                    break;
                }
                
            }

        } else {

            //Цикл добавляющий классы 'active' ко всем 'child' до выбранной зведзы
            for( var i = 0; i < element.children.length; ++i ){
                
                var childItem = element.children[i];
                
                //RemoveClass
                childItem.className = '';
                
                //AddClass
                childItem.classList.add(...classFull);

                if( childItem == target ) {
                    var box = childItem.getBoundingClientRect(),
                        boxLeftHalf = box.x + box.width / 2;

                    if( boxLeftHalf >= event.clientX ) {
                        childItem.className = '';
                        childItem.classList.add(...classHalf);
                        
                        inputValue = i + 0.5;
                    } else {
                        inputValue = i + 1;
                    }
                    break;
                }

            }

        }
    }
    element.nextElementSibling.value = inputValue;
} 
